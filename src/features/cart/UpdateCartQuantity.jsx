import Button from "../../ui/Button";
import {
  decreaseQuantitiy,
  getCurrentItemQuantity,
  increaseQuantitiy,
} from "./cartSlice";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import DeleteItem from "./DeleteItem";

const UpdateCartQuantity = ({ id }) => {
  const currentQuantity = useSelector(getCurrentItemQuantity(id));
  const dispatch = useDispatch();
  return (
    <div className="flex gap-x-4 items-center">
      <Button type={"rounded"} onClick={() => dispatch(increaseQuantitiy(id))}>
        +
      </Button>
      <p> {currentQuantity}</p>
      <Button type={"rounded"} onClick={() => dispatch(decreaseQuantitiy(id))}>
        -
      </Button>
    </div>
  );
};

export default UpdateCartQuantity;
